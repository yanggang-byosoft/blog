---

title: UEFI USB Driver Stack

date: 2022-12-10 10:24:00

tags:

---



<center> This Article Introduce the UEFI USB Driver Stack. </center>

*<!--more-->*

## UEFI USB Driver Stack

> 以下部分描述没有“直译”为中文，可能会更加方便去理解/追踪 [edk2](https://github.com/tianocore/edk2) 中的代码实现。

UEFI USB 协议栈主要的驱动为 **USB Host Controller Driver(XhciDxe)** **USB Bus Driver(UsbBusDxe)** **USB Device Driver(UsbKbDxe/UsbMassStorageDxe)**。下一层的驱动需要依赖上层驱动产生的协议，然后产生针对特定类型的 USB 设备的设备驱动，最终提供对键盘/鼠标/U盘等设备的控制/操作。

----

### UEFI Driver Binding Protocol

上述的各驱动均是符合 UEFI Driver Model 的驱动，对 UEFI 驱动模型有所了解，也可以更好的去理解上述各个驱动之间的关系。

在上述各个驱动的 EntryPoint 函数可以看到，他们都安装了 UEFI Driver Binding Protocol，这个 Protocol 提供了 `Supported()` `Start()` 和 `Stop()` 三个接口。下面会以 XhciDxe 为例子简述一下这几个接口。

```
// MdeModulePkg\Bus\Pci\XhciDxe\Xhci.c 中的 EFI_DRIVER_BINDING_PROTOCOL
EFI_DRIVER_BINDING_PROTOCOL  gXhciDriverBinding = {
  XhcDriverBindingSupported,
  XhcDriverBindingStart,
  XhcDriverBindingStop,
  0x30,
  NULL,
  NULL
};
```

#### Supported()

这个接口用来检测一个 Controller 是否支持该驱动，如果均支持则返回 `EFI_SUCCESS`，否则返回 `EFI_UNSUPPORTED`。

例如 XhciDxe 的 `XhcDriverBindingSupported` 首先会检查 PciIo Protocol(`gEfiPciIoProtocolGuid`)，然后通过 PciIo 获取 ClassCode，检查是否为 XHCI 类型的 USB 设备。

#### Start()

这个接口会把驱动安装到支持的设备 Controller 上，这里一般会调用 `gBS->InstallProtocolInterface` 或 `gBS->InstallMultipleProtocolInterfaces` 讲一个或多个 Protocol 安装到对应的 Controller Handle 上。

例如 XhciDxe 的 `XhcDriverBindingStart` 函数会使用如下代码安装 `gEfiUsb2HcProtocolGuid`：

```
Status = gBS->InstallProtocolInterface (
                &Controller,
                &gEfiUsb2HcProtocolGuid,
                EFI_NATIVE_INTERFACE,
                &Xhc->Usb2Hc
                );
```

#### Stop()

此接口会调用 `gBS->UninstallProtocolInterface` 或 `gBS->UninstallMultipleProtocolInterfaces` 接口将此驱动安装的 Protocol 卸载，并停掉相关服务（Timer/Event...等）。

----

### UEFI USB 各驱动之间的关系

**下图为 UEFI USB Driver Stack 的层级关系**

PCI Bus -> USB Host Controller -> USB Bus -> USB Device

![UEFI USB Driver Stack](https://3762986563-files.gitbook.io/~/files/v0/b/gitbook-legacy-files/o/assets%2F-M5spcWgajif4BDyesL5%2F-M5spgyciM5ILL7rFd39%2F-M5spoJt-JM4tiT422RH%2Fimage40.jpg?generation=1587944063052082&alt=media)

以下摘录自 "EDK II UEFI Driver Writer's Guide"，对下图或 UEFI USB 协议栈有一个大致的描述。

> In this example, the platform hardware provides a single USB host controller on the PCI bus. The PCI bus driver produces a handle with EFI_DEVICE_PATH_PROTOCOL and EFI_PCI_IO_PROTOCOL installed for this USB host controller. The USB host controller driver then consumes EFI_PCI_IO_PROTOCOL on that USB host controller device handle and installs the EFI_USB2_HC_PROTOCOL onto the same handle.

> The USB bus driver consumes the services of EFI_USB2_HC_PROTOCOL. It uses these services to enumerate the USB bus. In this example, the USB bus driver detects a USB keyboard, a USB mouse, and a USB mass storage device. As a result, the USB bus driver creates three child handles and installs the EFI_DEVICE_PATH_PROTOCOL and EFI_USB_IO_PROTOCOL onto each of those handles.

> The USB mouse driver consumes the EFI_USB_IO_PROTOCOL and produces the EFI_SIMPLE_POINTER_PROTOCOL. The USB keyboard driver consumes the EFI_USB_IO_PROTOCOL to produce the EFI_SIMPLE_TEXT_INPUT_PROTOCOL. The USB mass storage driver consumes the EFI_USB_IO_PROTOCOL to produce the EFI_USB_IO_PROTOCOL.

> The protocol interfaces for the USB2 Host Controller Protocol and the USB I/O Protocol are shown below in the following two examples.

**以下内容会主要针对各驱动的 Start() 函数进行介绍**

----

### USB Host Controller Driver (MdeModulePkg\Bus\Pci\XhciDxe)

主要的 USB Host Controller 有如下几类：

* Open Host Controller Interface (OHCI) (USB 1.0 and USB 1.1)
* Universal Host Controller Interface (UHCI) (USB 1.0 and USB 1.1)
* Enhanced Host Controller Interface (EHCI) (USB 2.0)
* Extended Host Controller Interface (XHCI) (USB 3.0)

USB Host Controller Driver 会调用 EFI_PCI_IO_PROTOCOL 并产生 EFI_USB2_HC_PROTOCOL 供 USB Bus Driver 使用。安装 EFI_USB2_HC_PROTOCOL 的地方便在 XhciDxe 的 `XhcDriverBindingStart()` 函数中。

Start() 函数中首先会根据传入的 Controller Handle 打开 PCI IO Protocol 和 DevicePath Protocol

```
  Status = gBS->OpenProtocol (
                  Controller,
                  &gEfiPciIoProtocolGuid,
                  (VOID **) &PciIo,
                  This->DriverBindingHandle,
                  Controller,
                  EFI_OPEN_PROTOCOL_BY_DRIVER
                  );

  Status = gBS->OpenProtocol (
                  Controller,
                  &gEfiDevicePathProtocolGuid,
                  (VOID **) &HcDevicePath,
                  This->DriverBindingHandle,
                  Controller,
                  EFI_OPEN_PROTOCOL_GET_PROTOCOL
                  );

```

然后通过 PciIo  Host Controller 对应的 PCI Attribute 保存下来以在后面 Exit Boot Service 切换 USB Ownership 时用来恢复。然后 enable the USB host controller

```
  //
  // Save original PCI attributes
  //
  Status = PciIo->Attributes (
                    PciIo,
                    EfiPciIoAttributeOperationGet,
                    0,
                    &OriginalPciAttributes // 会存到内部的 USB_XHCI_INSTANCE 中
                    );

  // enable the USB host controller
  Status = PciIo->Attributes (
                    PciIo,
                    EfiPciIoAttributeOperationSupported,
                    0,
                    &Supports
                    );
  if (!EFI_ERROR (Status)) {
    Supports &= (UINT64)EFI_PCI_DEVICE_ENABLE;
    Status = PciIo->Attributes (
                      PciIo,
                      EfiPciIoAttributeOperationEnable,
                      Supports,
                      NULL
                      );
  }
```


`XhcCreateUsbHc()` 函数