---

title: How to Build Tailored WebKit Engine

date: 2022-08-24 12:30:00

tags:

---



<center> How to Build Tailored WebKit Engine </center>

*<!--more-->*

## How to Build Tailored WebKit Engine

### Enviroment

> Pop!_OS 22.04 LTS (based on Ubuntu)

### Install build tools and librarys

Step by Step

```
sudo apt install cmake
sudo apt install ninja-build
sudo apt install ruby3.0-dev
sudo apt search ccache(Optional:Compiler cache for fast recompilation of C/C++ code)
sudo apt install libcairo2-dev
sudo apt install libgcrypt20-dev
sudo apt install libharfbuzz-dev
sudo apt install libjpeg-dev
sudo apt install libsqlite3-dev
sudo apt install libatk1.0-dev
sudo apt install libwebp-dev
sudo apt install libatspi2.0-dev
sudo apt install libgtk-3-dev
sudo apt install libxslt1-dev
sudo apt install libsecret-1-dev
sudo apt install libtasn1-6-dev
sudo apt install libxt-dev
sudo apt install libxt-dev
sudo apt install libnotify-dev
sudo apt install libhyphen-dev
sudo apt install libopenjp2-7-dev
sudo apt install libwoff-dev
sudo apt install libsystemd-dev
sudo apt install libseccomp-dev
sudo apt install libges-1.0-dev
sudo apt install gperf
sudo apt install gettext
sudo apt install libenchant-2-dev
```
Or

```
sudo apt install cmake ninja-build ruby3.0-dev ccache libcairo2-dev libgcrypt20-dev libharfbuzz-dev libjpeg-dev libsqlite3-dev libatk1.0-dev libwebp-dev libatspi2.0-dev libgtk-3-dev libxslt1-dev libsecret-1-dev libtasn1-6-dev libxt-dev libxt-dev libnotify-dev libhyphen-dev libopenjp2-7-dev libwoff-dev libsystemd-dev libseccomp-dev libges-1.0-dev gperf gettext libenchant-2-dev
```

**特别说明**
libsoup 库不要两个版本都安装，后续编译/运行 xGUI-Pro 大概会遇到冲突问题
sudo apt install libsoup-3.0-dev(-DUSE_SOUP3=ON) or libsoup2.4-dev(-DUSE_SOUP2=ON)

以下说明使用的是 libsoup3，所以我会禁用 libsoup2 (-DUSE_SOUP2=OFF -DUSE_SOUP3=ON)

### Build Tailored WebKit Engine

1. Get [Tailored WebKit Engine](https://files.fmsoft.cn/hvml/webkitgtk-2.34.1-hvml-220804.tar.bz2) from [xGUI-Pro](https://github.com/HVML/xGUI-Pro) repo readme link.

2. Extract this source package:

    ```
    tar -jxvf webkitgtk-2.34.1-hvml-220804.tar.bz2
    ```

3. Build

    ```
    cd webkitgtk-2.34.1

    mkdir -p WebKitBuild/Release && cd WebKitBuild/Release
    ```

    ```
    cmake -DPORT=GTK -DCMAKE_BUILD_TYPE=RelWithDebInfo -DENABLE_HVML_ATTRS=ON -DENABLE_GAMEPAD=OFF -DENABLE_INTROSPECTION=OFF -DUSE_WPE_RENDERER=OFF -DUSE_LCMS=OFF -DUSE_SOUP2=OFF -DUSE_SOUP3=ON -GNinja
    ```

    ```
    ninja
    ```

4. Install

    ```
    sudo ninja install
    ```
