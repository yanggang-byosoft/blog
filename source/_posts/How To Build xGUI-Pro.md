---

title: How to Build xGUI-Pro

date: 2022-08-24 12:30:00

tags:

---



<center> How to Build xGUI-Pro </center>

*<!--more-->*
## How to Build xGUI-Pro

### Enviroment

> Pop!_OS 22.04 LTS (based on Ubuntu)

### Dependencies

[PurC](https://github.com/HVML/PurC)
> You can refer to `How To Build PurC.md` to build it.

[DOM Ruler](https://github.com/HVML/DOM-Ruler)
> Just follow the steps in this repo to build it.

You have to use [Tailored WebKit Engine](https://files.fmsoft.cn/hvml/webkitgtk-2.34.1-hvml-220804.tar.bz2) to support two HVML-specific attributes hvml-handle and hvml-events and other requirements.
> You can refer to `How to Build Tailored WebKit Engine.md`

以下依赖可能缺失，按需安装：

```
sudo apt install libssl-dev
```

由于在之前编译定制版 WebKit 时我们使用的是 libsoup3，所以在编译 xGUI-Pro 时选择禁用 libsoup2 (-DUSE_SOUP2=OFF)

### Build xGUI-Pro

1. First you have to build and install [PurC](https://github.com/HVML/PurC), [DOM Ruler](https://github.com/HVML/DOM-Ruler), [Tailored WebKit Engine](https://files.fmsoft.cn/hvml/webkitgtk-2.34.1-hvml-220804.tar.bz2)

2. Get xGUI-Pro source code
   
   ```
   git clone https://github.com/HVML/xGUI-Pro.git
   ```

3. Build & Install

    ```
    cd xGUI-Pro
    sudo rm -rf build && sudo cmake -DCMAKE_BUILD_TYPE=Debug -DPORT=GTK -DUSE_SOUP2=OFF -B build && sudo cmake --build build
    cd build/
    sudo make install
    ```

3. Test

    ```
    xguipro -v
    ```

    After this command, you should see the version information like `WebKitGTK 2.34.1 (tarball.HVML)` in the terminal.

    ```
    xguipro
    ```
    After this command, you should see something in your display^_^.
