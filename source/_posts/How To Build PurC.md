---

title: How to Build PurC

date: 2022-08-24 12:30:00

tags:

---



<center> How to Build PurC </center>

*<!--more-->*

## How to Build PurC

### Enviroment

> Pop!_OS 22.04 LTS (based on Ubuntu)

### Install build tools and librarys(Minimum)

Step by Step

```
sudo apt install bison
sudo apt install flex
```
Or

```
sudo apt install bison flex
```

### Build PurC

1. Get PurC source code
   
   ```
   git clone https://github.com/HVML/PurC.git
   ```

2. Build & Install

    ```
    cd PurC
    sudo rm -rf build/
    sudo mkdir build/
    cd build/
    sudo cmake -DCMAKE_BUILD_TYPE=Release -DPORT=Linux ..
    sudo make -j4
    sudo make install
    ```

3. Optional operation
   
   Make sure the shared libraries can be find.
   ```
   sudo ldconfig
   ```

4. Test

    ```
    purc -v
    ```

    After this command, you should see the version information like `purc 0.8.1` in the terminal.

    ```
    purc hvml/hello.hvml
    ```

    After this command, you should see `Hello, world!` in the terminal.

5. HVML GUI Test
   首先，需要编译&安装好定制的 WebKit，xGUI-Pro，DOM Ruler
   然后，在一个终端中执行 `xguipro`，不要关闭 xguipro，新开一个终端来运行 HVML GUI 程序，如计算器程序：
   使用如下命令：
   ```
   purc -p purcmc -b calculator-bc.hvml
   ```
   不出意外，你会看到计算器的界面^_^